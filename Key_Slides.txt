Overview & Capabilities of VCS
1. What it does
2. How it works
  a. Decentralized subsets

Basic Principles
1.Vocabulary
  a. Appendix of key terms for reference; Nouns & Verbs
2. file storage type agnostic

Decentralized Code Storage
1. each user is subset of origin/master; code is local until commit & push

Workflows
1. GitFlow
2. Feature
3. Combined interactions

Permissions
1. options available @ per user, repo, branch

Cost & Overhead
1. Dataload
  a. Dependent on source code format
  b. File structure (easy)
  c. Database records (hard)

Extras
a. Webhooks - automation around trigger & event (https://confluence.atlassian.com/bitbucket/manage-webhooks-735643732.html)
b. Integration w/ JIRA
c. Bitbucket Data Center
d. Documentation version storage
e. Large File Storage